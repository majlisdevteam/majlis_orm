/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dmssw.orm.controllers;

import com.dmssw.orm.config.HibernateUtil;
import java.io.Serializable;
import java.util.List;
import java.util.Map;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.query.Query;

/**
 *
 * @author Shanka
 */
public class ORMConnection {

    /**
     * session start and begin transactions
     *
     * @return session variable with began transaction
     */
    public Session beginTransaction() {

        Session session = null;
        try {
            session = HibernateUtil.getSessionFactory().openSession();
            session.beginTransaction();
        } catch (Exception e) {
            System.out.println("Cannot open session. Error : " + e.getMessage());
            e.printStackTrace();
        }
        return session;
    }

    /**
     *
     * @param session started session
     * @param obj object that want to save
     * @return last return id
     */
    public int createObject(Session session, Object obj) {
        int lastRow = -1;
        try {
            if (session != null) {

                Serializable ser = session.save(obj);

                if (ser != null) {
                    lastRow = (Integer) ser;
                }

            } else {
                System.err.println("Object cannot create. Session is null");
                throw new HibernateException("Object cannot create. Session is null");
            }

        } catch (HibernateException e) {
            session.getTransaction().rollback();
            session = null;

            System.err.println("Object cannot create. Error :  " + e.getMessage());

            e.printStackTrace();

        }

        return lastRow;
    }

    /**
     *
     * @param session session that started
     * @param obj object that want to update
     */
    public Session updateObject(Session session, Object obj) {
        try {

            if (session != null) {
//                session.update(obj);
                System.out.println(">>>>>>>>>>>>>>>>>>>>>>>>");
                session.saveOrUpdate(obj);
            } else {
                System.err.println("Object cannot update. Session is null");
                throw new HibernateException("Object cannot update. Session is null");
            }

        } catch (Exception e) {
            session.getTransaction().rollback();
            session = null;

            System.err.println("Object cannot update. Error :  " + e.getMessage());

            e.printStackTrace();

        }
        return session;
    }

    /**
     *
     * @param session session that created
     * @return value : 1 => complete, 2 => Session is null, 3=> Error occur
     */
    public int commitObject(Session session) {

        int result = 0;

        try {
            if (session != null) {
                session.getTransaction().commit();

                result = 1;

            } else {
                result = 2;
                System.err.println("Object cannot commit. Session is null");
                throw new HibernateException("Object cannot commit. Session is null");
            }

        } catch (Exception e) {

            result = 3;

            session.getTransaction().rollback();

            System.err.println("Object cannot commit. Error :  " + e.getMessage());

            e.printStackTrace();
        } finally {
            if (session != null) {

                session.close();

            }
        }

        return result;
    }

    /**
     *
     * @param hql
     * @return List<Objet> of result
     */
    public List<Object> hqlGetResults(String hql) {

        Session session = null;
        try {

            session = HibernateUtil.getSessionFactory().openSession();

            Query query = session.createQuery(hql);

            System.out.println("LLLLLLLL");

            return query.list();
        } catch (HibernateException e) {
            e.printStackTrace();
            throw new HibernateException(e.getMessage());
        } finally {
            if (session != null) {
//                session.close();
            }
        }

    }

    /**
     *
     * @param hql
     * @param parameterList
     * @return List<Objet> of result
     */
    public List<Object> hqlGetResults(String hql, Map<String, Object> parameterList) {

        Session session = null;
        try {

            session = HibernateUtil.getSessionFactory().openSession();

            Query query = session.createQuery(hql);

            parameterList.forEach((key, value) -> {

                System.out.println("key " + key + " value " + value);
                query.setParameter(key, value);
            });
            return query.list();
        } catch (HibernateException e) {
            e.printStackTrace();
            throw new HibernateException(e.getMessage());
        } finally {
            if (session != null) {

                session.close();

            }
        }

    }

    /**
     *
     * @param session
     * @param hql
     * @return number of affected rows
     */
    public int hqlUpdate(Session session, String hql) {

        try {

            session = HibernateUtil.getSessionFactory().openSession();

            Query query = session.createQuery(hql);

            return query.executeUpdate();

        } catch (HibernateException e) {

            e.printStackTrace();

            throw new HibernateException(e.getMessage());

        } finally {

            if (session != null) {

                session.close();

            }

        }

    }

    /**
     *
     * @param session
     * @param hql
     * @param parameterList
     * @return number of affected rows
     */
    public int hqlUpdate(Session session, String hql, Map<String, Object> parameterList) {

        try {

            session = HibernateUtil.getSessionFactory().openSession();

            Query query = session.createQuery(hql);

            parameterList.forEach((key, value) -> {

                System.out.println("key " + key + " value " + value);
                query.setParameter(key, value);
            });

            return query.executeUpdate();

        } catch (HibernateException e) {

            e.printStackTrace();

            throw new HibernateException(e.getMessage());

        } finally {

            if (session != null) {

                session.close();

            }

        }

    }

    /**
     *
     * @param session
     * @param hql
     * @return number of affected rows
     */
    public int hqlDelete(Session session, String hql) {

        try {

            session = HibernateUtil.getSessionFactory().openSession();

            Query query = session.createQuery(hql);

            return query.executeUpdate();

        } catch (HibernateException e) {

            e.printStackTrace();

            throw new HibernateException(e.getMessage());

        } finally {

            if (session != null) {

                session.close();

            }

        }

    }

    /**
     *
     * @param session
     * @param hql
     * @param parameterList
     * @return number of affected rows
     */
    public int hqlDelete(Session session, String hql, Map<String, Object> parameterList) {

        try {

            session = HibernateUtil.getSessionFactory().openSession();

            Query query = session.createQuery(hql);

            parameterList.forEach((key, value) -> {

                System.out.println("key " + key + " value " + value);
                query.setParameter(key, value);
            });

            return query.executeUpdate();

        } catch (HibernateException e) {

            e.printStackTrace();

            throw new HibernateException(e.getMessage());

        } finally {

            if (session != null) {

                session.close();

            }

        }

    }

    /**
     *
     * @param session
     * @param hql
     * @return number of affected rows
     */
    public int hqlInsert(Session session, String hql) {

        try {

            session = HibernateUtil.getSessionFactory().openSession();

            Query query = session.createQuery(hql);

            return query.executeUpdate();

        } catch (HibernateException e) {

            e.printStackTrace();

            throw new HibernateException(e.getMessage());

        } finally {

            if (session != null) {

                session.close();

            }

        }

    }
}
