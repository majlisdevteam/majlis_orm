/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dmssw.orm.models;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Shanka
 */
@Entity
@Table(name = "majlis_category")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "MajlisCategory.findAll", query = "SELECT m FROM MajlisCategory m"),
    @NamedQuery(name = "MajlisCategory.findByCategoryId", query = "SELECT m FROM MajlisCategory m WHERE m.categoryId = :categoryId"),
    @NamedQuery(name = "MajlisCategory.findByCategorySystemId", query = "SELECT m FROM MajlisCategory m WHERE m.categorySystemId = :categorySystemId"),
    @NamedQuery(name = "MajlisCategory.findByCategoryTitle", query = "SELECT m FROM MajlisCategory m WHERE m.categoryTitle = :categoryTitle"),
    @NamedQuery(name = "MajlisCategory.findByCategoryIconPath", query = "SELECT m FROM MajlisCategory m WHERE m.categoryIconPath = :categoryIconPath"),
    @NamedQuery(name = "MajlisCategory.findByCategoryDescription", query = "SELECT m FROM MajlisCategory m WHERE m.categoryDescription = :categoryDescription"),
    @NamedQuery(name = "MajlisCategory.findByUserInserted", query = "SELECT m FROM MajlisCategory m WHERE m.userInserted = :userInserted"),
    @NamedQuery(name = "MajlisCategory.findByDateInserted", query = "SELECT m FROM MajlisCategory m WHERE m.dateInserted = :dateInserted"),
    @NamedQuery(name = "MajlisCategory.findByUserModified", query = "SELECT m FROM MajlisCategory m WHERE m.userModified = :userModified"),
    @NamedQuery(name = "MajlisCategory.findByDateModified", query = "SELECT m FROM MajlisCategory m WHERE m.dateModified = :dateModified")})
public class MajlisCategory implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "CATEGORY_ID")
    private Integer categoryId;
    @Size(max = 50)
    @Column(name = "CATEGORY_SYSTEM_ID")
    private String categorySystemId;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 200)
    @Column(name = "CATEGORY_TITLE")
    private String categoryTitle;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 2000)
    @Column(name = "CATEGORY_ICON_PATH")
    private String categoryIconPath;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 2000)
    @Column(name = "CATEGORY_DESCRIPTION")
    private String categoryDescription;
    @Size(max = 50)
    @Column(name = "USER_INSERTED")
    private String userInserted;
    @Column(name = "DATE_INSERTED")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateInserted;
    @Size(max = 50)
    @Column(name = "USER_MODIFIED")
    private String userModified;
    @Column(name = "DATE_MODIFIED")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateModified;
    @JoinTable(name = "majlis_mobile_users_category", joinColumns = {
        @JoinColumn(name = "CATEGORY_ID", referencedColumnName = "CATEGORY_ID")}, inverseJoinColumns = {
        @JoinColumn(name = "USER_ID", referencedColumnName = "MOBILE_USER_ID")})
    @ManyToMany
    private List<MajlisMobileUsers> majlisMobileUsersList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "eventCategory")
    private List<MajlisGroupEvent> majlisGroupEventList;
    @JoinColumn(name = "CATEGORY_STATUS", referencedColumnName = "CODE_ID")
    @ManyToOne(optional = false)
    private MajlisMdCode categoryStatus;

    public MajlisCategory() {
    }

    public MajlisCategory(Integer categoryId) {
        this.categoryId = categoryId;
    }

    public MajlisCategory(Integer categoryId, String categoryTitle, String categoryIconPath, String categoryDescription) {
        this.categoryId = categoryId;
        this.categoryTitle = categoryTitle;
        this.categoryIconPath = categoryIconPath;
        this.categoryDescription = categoryDescription;
    }

    public Integer getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(Integer categoryId) {
        this.categoryId = categoryId;
    }

    public String getCategorySystemId() {
        return categorySystemId;
    }

    public void setCategorySystemId(String categorySystemId) {
        this.categorySystemId = categorySystemId;
    }

    public String getCategoryTitle() {
        return categoryTitle;
    }

    public void setCategoryTitle(String categoryTitle) {
        this.categoryTitle = categoryTitle;
    }

    public String getCategoryIconPath() {
        return categoryIconPath;
    }

    public void setCategoryIconPath(String categoryIconPath) {
        this.categoryIconPath = categoryIconPath;
    }

    public String getCategoryDescription() {
        return categoryDescription;
    }

    public void setCategoryDescription(String categoryDescription) {
        this.categoryDescription = categoryDescription;
    }

    public String getUserInserted() {
        return userInserted;
    }

    public void setUserInserted(String userInserted) {
        this.userInserted = userInserted;
    }

    public Date getDateInserted() {
        return dateInserted;
    }

    public void setDateInserted(Date dateInserted) {
        this.dateInserted = dateInserted;
    }

    public String getUserModified() {
        return userModified;
    }

    public void setUserModified(String userModified) {
        this.userModified = userModified;
    }

    public Date getDateModified() {
        return dateModified;
    }

    public void setDateModified(Date dateModified) {
        this.dateModified = dateModified;
    }

    @XmlTransient
    public List<MajlisMobileUsers> getMajlisMobileUsersList() {
        return majlisMobileUsersList;
    }

    public void setMajlisMobileUsersList(List<MajlisMobileUsers> majlisMobileUsersList) {
        this.majlisMobileUsersList = majlisMobileUsersList;
    }

    @XmlTransient
    public List<MajlisGroupEvent> getMajlisGroupEventList() {
        return majlisGroupEventList;
    }

    public void setMajlisGroupEventList(List<MajlisGroupEvent> majlisGroupEventList) {
        this.majlisGroupEventList = majlisGroupEventList;
    }

    public MajlisMdCode getCategoryStatus() {
        return categoryStatus;
    }

    public void setCategoryStatus(MajlisMdCode categoryStatus) {
        this.categoryStatus = categoryStatus;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (categoryId != null ? categoryId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof MajlisCategory)) {
            return false;
        }
        MajlisCategory other = (MajlisCategory) object;
        if ((this.categoryId == null && other.categoryId != null) || (this.categoryId != null && !this.categoryId.equals(other.categoryId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.dmssw.orm.models.MajlisCategory[ categoryId=" + categoryId + " ]";
    }
    
}
