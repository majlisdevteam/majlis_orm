/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dmssw.orm.models;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Shanka
 */
@Entity
@Table(name = "majlis_group_event_videos")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "MajlisGroupEventVideos.findAll", query = "SELECT m FROM MajlisGroupEventVideos m"),
    @NamedQuery(name = "MajlisGroupEventVideos.findByVideoId", query = "SELECT m FROM MajlisGroupEventVideos m WHERE m.videoId = :videoId"),
    @NamedQuery(name = "MajlisGroupEventVideos.findByVideoFilePath", query = "SELECT m FROM MajlisGroupEventVideos m WHERE m.videoFilePath = :videoFilePath"),
    @NamedQuery(name = "MajlisGroupEventVideos.findByUserInserted", query = "SELECT m FROM MajlisGroupEventVideos m WHERE m.userInserted = :userInserted"),
    @NamedQuery(name = "MajlisGroupEventVideos.findByDateInserted", query = "SELECT m FROM MajlisGroupEventVideos m WHERE m.dateInserted = :dateInserted")})
public class MajlisGroupEventVideos implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "VIDEO_ID")
    private Integer videoId;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 2000)
    @Column(name = "VIDEO_FILE_PATH")
    private String videoFilePath;
    @Column(name = "USER_INSERTED")
    private Integer userInserted;
    @Column(name = "DATE_INSERTED")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateInserted;
    @JoinColumn(name = "EVENT_ID", referencedColumnName = "EVENT_ID")
    @ManyToOne
    private MajlisGroupEvent eventId;

    public MajlisGroupEventVideos() {
    }

    public MajlisGroupEventVideos(Integer videoId) {
        this.videoId = videoId;
    }

    public MajlisGroupEventVideos(Integer videoId, String videoFilePath) {
        this.videoId = videoId;
        this.videoFilePath = videoFilePath;
    }

    public Integer getVideoId() {
        return videoId;
    }

    public void setVideoId(Integer videoId) {
        this.videoId = videoId;
    }

    public String getVideoFilePath() {
        return videoFilePath;
    }

    public void setVideoFilePath(String videoFilePath) {
        this.videoFilePath = videoFilePath;
    }

    public Integer getUserInserted() {
        return userInserted;
    }

    public void setUserInserted(Integer userInserted) {
        this.userInserted = userInserted;
    }

    public Date getDateInserted() {
        return dateInserted;
    }

    public void setDateInserted(Date dateInserted) {
        this.dateInserted = dateInserted;
    }

    public MajlisGroupEvent getEventId() {
        return eventId;
    }

    public void setEventId(MajlisGroupEvent eventId) {
        this.eventId = eventId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (videoId != null ? videoId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof MajlisGroupEventVideos)) {
            return false;
        }
        MajlisGroupEventVideos other = (MajlisGroupEventVideos) object;
        if ((this.videoId == null && other.videoId != null) || (this.videoId != null && !this.videoId.equals(other.videoId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.dmssw.orm.models.MajlisGroupEventVideos[ videoId=" + videoId + " ]";
    }
    
}
