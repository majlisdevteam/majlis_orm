/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dmssw.orm.models;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Shanka
 */
@Entity
@Table(name = "majlis_group_event")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "MajlisGroupEvent.findAll", query = "SELECT m FROM MajlisGroupEvent m"),
    @NamedQuery(name = "MajlisGroupEvent.findByEventId", query = "SELECT m FROM MajlisGroupEvent m WHERE m.eventId = :eventId"),
    @NamedQuery(name = "MajlisGroupEvent.findByEventSystemId", query = "SELECT m FROM MajlisGroupEvent m WHERE m.eventSystemId = :eventSystemId"),
    @NamedQuery(name = "MajlisGroupEvent.findByEventCaption", query = "SELECT m FROM MajlisGroupEvent m WHERE m.eventCaption = :eventCaption"),
    @NamedQuery(name = "MajlisGroupEvent.findByEventIconPath", query = "SELECT m FROM MajlisGroupEvent m WHERE m.eventIconPath = :eventIconPath"),
    @NamedQuery(name = "MajlisGroupEvent.findByEventMessage", query = "SELECT m FROM MajlisGroupEvent m WHERE m.eventMessage = :eventMessage"),
    @NamedQuery(name = "MajlisGroupEvent.findByEventLocation", query = "SELECT m FROM MajlisGroupEvent m WHERE m.eventLocation = :eventLocation"),
    @NamedQuery(name = "MajlisGroupEvent.findByEventLocationLat", query = "SELECT m FROM MajlisGroupEvent m WHERE m.eventLocationLat = :eventLocationLat"),
    @NamedQuery(name = "MajlisGroupEvent.findByEventLocationLont", query = "SELECT m FROM MajlisGroupEvent m WHERE m.eventLocationLont = :eventLocationLont"),
    @NamedQuery(name = "MajlisGroupEvent.findByEventTime", query = "SELECT m FROM MajlisGroupEvent m WHERE m.eventTime = :eventTime"),
    @NamedQuery(name = "MajlisGroupEvent.findByUserInserted", query = "SELECT m FROM MajlisGroupEvent m WHERE m.userInserted = :userInserted"),
    @NamedQuery(name = "MajlisGroupEvent.findByDateInserted", query = "SELECT m FROM MajlisGroupEvent m WHERE m.dateInserted = :dateInserted"),
    @NamedQuery(name = "MajlisGroupEvent.findByUserModified", query = "SELECT m FROM MajlisGroupEvent m WHERE m.userModified = :userModified"),
    @NamedQuery(name = "MajlisGroupEvent.findByDateModified", query = "SELECT m FROM MajlisGroupEvent m WHERE m.dateModified = :dateModified")})
public class MajlisGroupEvent implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "EVENT_ID")
    private Integer eventId;
    @Size(max = 50)
    @Column(name = "EVENT_SYSTEM_ID")
    private String eventSystemId;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 11)
    @Column(name = "EVENT_CAPTION")
    private String eventCaption;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 2000)
    @Column(name = "EVENT_ICON_PATH")
    private String eventIconPath;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 5000)
    @Column(name = "EVENT_MESSAGE")
    private String eventMessage;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 2000)
    @Column(name = "EVENT_LOCATION")
    private String eventLocation;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "EVENT_LOCATION_LAT")
    private String eventLocationLat;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "EVENT_LOCATION_LONT")
    private String eventLocationLont;
    @Basic(optional = false)
    @NotNull
    @Column(name = "EVENT_TIME")
    @Temporal(TemporalType.TIMESTAMP)
    private Date eventTime;
    @Size(max = 50)
    @Column(name = "USER_INSERTED")
    private String userInserted;
    @Column(name = "DATE_INSERTED")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateInserted;
    @Size(max = 50)
    @Column(name = "USER_MODIFIED")
    private String userModified;
    @Column(name = "DATE_MODIFIED")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateModified;
    @JoinColumn(name = "EVENT_CATEGORY", referencedColumnName = "CATEGORY_ID")
    @ManyToOne(optional = false)
    private MajlisCategory eventCategory;
    @JoinColumn(name = "EVENT_STATUS", referencedColumnName = "CODE_ID")
    @ManyToOne(optional = false)
    private MajlisMdCode eventStatus;
    @JoinColumn(name = "GROUP_ID", referencedColumnName = "GROUP_ID")
    @ManyToOne(optional = false)
    private MajlisGroup groupId;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "notificationEventId")
    private List<MajlisNotification> majlisNotificationList;
    @OneToMany(mappedBy = "eventId")
    private List<MajlisGroupEventVideos> majlisGroupEventVideosList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "commentEventId")
    private List<MajlisEventComment> majlisEventCommentList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "eventId")
    private List<MajlisGroupEventImages> majlisGroupEventImagesList;

    public MajlisGroupEvent() {
    }

    public MajlisGroupEvent(Integer eventId) {
        this.eventId = eventId;
    }

    public MajlisGroupEvent(Integer eventId, String eventCaption, String eventIconPath, String eventMessage, String eventLocation, String eventLocationLat, String eventLocationLont, Date eventTime) {
        this.eventId = eventId;
        this.eventCaption = eventCaption;
        this.eventIconPath = eventIconPath;
        this.eventMessage = eventMessage;
        this.eventLocation = eventLocation;
        this.eventLocationLat = eventLocationLat;
        this.eventLocationLont = eventLocationLont;
        this.eventTime = eventTime;
    }

    public Integer getEventId() {
        return eventId;
    }

    public void setEventId(Integer eventId) {
        this.eventId = eventId;
    }

    public String getEventSystemId() {
        return eventSystemId;
    }

    public void setEventSystemId(String eventSystemId) {
        this.eventSystemId = eventSystemId;
    }

    public String getEventCaption() {
        return eventCaption;
    }

    public void setEventCaption(String eventCaption) {
        this.eventCaption = eventCaption;
    }

    public String getEventIconPath() {
        return eventIconPath;
    }

    public void setEventIconPath(String eventIconPath) {
        this.eventIconPath = eventIconPath;
    }

    public String getEventMessage() {
        return eventMessage;
    }

    public void setEventMessage(String eventMessage) {
        this.eventMessage = eventMessage;
    }

    public String getEventLocation() {
        return eventLocation;
    }

    public void setEventLocation(String eventLocation) {
        this.eventLocation = eventLocation;
    }

    public String getEventLocationLat() {
        return eventLocationLat;
    }

    public void setEventLocationLat(String eventLocationLat) {
        this.eventLocationLat = eventLocationLat;
    }

    public String getEventLocationLont() {
        return eventLocationLont;
    }

    public void setEventLocationLont(String eventLocationLont) {
        this.eventLocationLont = eventLocationLont;
    }

    public Date getEventTime() {
        return eventTime;
    }

    public void setEventTime(Date eventTime) {
        this.eventTime = eventTime;
    }

    public String getUserInserted() {
        return userInserted;
    }

    public void setUserInserted(String userInserted) {
        this.userInserted = userInserted;
    }

    public Date getDateInserted() {
        return dateInserted;
    }

    public void setDateInserted(Date dateInserted) {
        this.dateInserted = dateInserted;
    }

    public String getUserModified() {
        return userModified;
    }

    public void setUserModified(String userModified) {
        this.userModified = userModified;
    }

    public Date getDateModified() {
        return dateModified;
    }

    public void setDateModified(Date dateModified) {
        this.dateModified = dateModified;
    }

    public MajlisCategory getEventCategory() {
        return eventCategory;
    }

    public void setEventCategory(MajlisCategory eventCategory) {
        this.eventCategory = eventCategory;
    }

    public MajlisMdCode getEventStatus() {
        return eventStatus;
    }

    public void setEventStatus(MajlisMdCode eventStatus) {
        this.eventStatus = eventStatus;
    }

    public MajlisGroup getGroupId() {
        return groupId;
    }

    public void setGroupId(MajlisGroup groupId) {
        this.groupId = groupId;
    }

    @XmlTransient
    public List<MajlisNotification> getMajlisNotificationList() {
        return majlisNotificationList;
    }

    public void setMajlisNotificationList(List<MajlisNotification> majlisNotificationList) {
        this.majlisNotificationList = majlisNotificationList;
    }

    @XmlTransient
    public List<MajlisGroupEventVideos> getMajlisGroupEventVideosList() {
        return majlisGroupEventVideosList;
    }

    public void setMajlisGroupEventVideosList(List<MajlisGroupEventVideos> majlisGroupEventVideosList) {
        this.majlisGroupEventVideosList = majlisGroupEventVideosList;
    }

    @XmlTransient
    public List<MajlisEventComment> getMajlisEventCommentList() {
        return majlisEventCommentList;
    }

    public void setMajlisEventCommentList(List<MajlisEventComment> majlisEventCommentList) {
        this.majlisEventCommentList = majlisEventCommentList;
    }

    @XmlTransient
    public List<MajlisGroupEventImages> getMajlisGroupEventImagesList() {
        return majlisGroupEventImagesList;
    }

    public void setMajlisGroupEventImagesList(List<MajlisGroupEventImages> majlisGroupEventImagesList) {
        this.majlisGroupEventImagesList = majlisGroupEventImagesList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (eventId != null ? eventId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof MajlisGroupEvent)) {
            return false;
        }
        MajlisGroupEvent other = (MajlisGroupEvent) object;
        if ((this.eventId == null && other.eventId != null) || (this.eventId != null && !this.eventId.equals(other.eventId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.dmssw.orm.models.MajlisGroupEvent[ eventId=" + eventId + " ]";
    }
    
}
