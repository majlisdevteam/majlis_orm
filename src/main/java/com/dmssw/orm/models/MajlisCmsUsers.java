/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dmssw.orm.models;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Shanka
 */
@Entity
@Table(name = "majlis_cms_users")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "MajlisCmsUsers.findAll", query = "SELECT m FROM MajlisCmsUsers m"),
    @NamedQuery(name = "MajlisCmsUsers.findByUserId", query = "SELECT m FROM MajlisCmsUsers m WHERE m.userId = :userId"),
    @NamedQuery(name = "MajlisCmsUsers.findByUserFullName", query = "SELECT m FROM MajlisCmsUsers m WHERE m.userFullName = :userFullName"),
    @NamedQuery(name = "MajlisCmsUsers.findByExpiryDate", query = "SELECT m FROM MajlisCmsUsers m WHERE m.expiryDate = :expiryDate"),
    @NamedQuery(name = "MajlisCmsUsers.findByNoAllowedNotifications", query = "SELECT m FROM MajlisCmsUsers m WHERE m.noAllowedNotifications = :noAllowedNotifications"),
    @NamedQuery(name = "MajlisCmsUsers.findByUserInserted", query = "SELECT m FROM MajlisCmsUsers m WHERE m.userInserted = :userInserted"),
    @NamedQuery(name = "MajlisCmsUsers.findByDateInserted", query = "SELECT m FROM MajlisCmsUsers m WHERE m.dateInserted = :dateInserted"),
    @NamedQuery(name = "MajlisCmsUsers.findByUserModified", query = "SELECT m FROM MajlisCmsUsers m WHERE m.userModified = :userModified"),
    @NamedQuery(name = "MajlisCmsUsers.findByDateModified", query = "SELECT m FROM MajlisCmsUsers m WHERE m.dateModified = :dateModified")})
public class MajlisCmsUsers implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "USER_ID")
    private String userId;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 120)
    @Column(name = "USER_FULL_NAME")
    private String userFullName;
    @Basic(optional = false)
    @NotNull
    @Column(name = "EXPIRY_DATE")
    @Temporal(TemporalType.DATE)
    private Date expiryDate;
    @Basic(optional = false)
    @NotNull
    @Column(name = "NO_ALLOWED_NOTIFICATIONS")
    private int noAllowedNotifications;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "USER_INSERTED")
    private String userInserted;
    @Column(name = "DATE_INSERTED")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateInserted;
    @Size(max = 50)
    @Column(name = "USER_MODIFIED")
    private String userModified;
    @Column(name = "DATE_MODIFIED")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateModified;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "notificationUserId")
    private List<MajlisNotification> majlisNotificationList;
    @JoinColumn(name = "STATUS", referencedColumnName = "CODE_ID")
    @ManyToOne(optional = false)
    private MajlisMdCode status;
    @OneToMany(mappedBy = "groupAdmin")
    private List<MajlisGroup> majlisGroupList;

    public MajlisCmsUsers() {
    }

    public MajlisCmsUsers(String userId) {
        this.userId = userId;
    }

    public MajlisCmsUsers(String userId, String userFullName, Date expiryDate, int noAllowedNotifications, String userInserted) {
        this.userId = userId;
        this.userFullName = userFullName;
        this.expiryDate = expiryDate;
        this.noAllowedNotifications = noAllowedNotifications;
        this.userInserted = userInserted;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getUserFullName() {
        return userFullName;
    }

    public void setUserFullName(String userFullName) {
        this.userFullName = userFullName;
    }

    public Date getExpiryDate() {
        return expiryDate;
    }

    public void setExpiryDate(Date expiryDate) {
        this.expiryDate = expiryDate;
    }

    public int getNoAllowedNotifications() {
        return noAllowedNotifications;
    }

    public void setNoAllowedNotifications(int noAllowedNotifications) {
        this.noAllowedNotifications = noAllowedNotifications;
    }

    public String getUserInserted() {
        return userInserted;
    }

    public void setUserInserted(String userInserted) {
        this.userInserted = userInserted;
    }

    public Date getDateInserted() {
        return dateInserted;
    }

    public void setDateInserted(Date dateInserted) {
        this.dateInserted = dateInserted;
    }

    public String getUserModified() {
        return userModified;
    }

    public void setUserModified(String userModified) {
        this.userModified = userModified;
    }

    public Date getDateModified() {
        return dateModified;
    }

    public void setDateModified(Date dateModified) {
        this.dateModified = dateModified;
    }

    @XmlTransient
    public List<MajlisNotification> getMajlisNotificationList() {
        return majlisNotificationList;
    }

    public void setMajlisNotificationList(List<MajlisNotification> majlisNotificationList) {
        this.majlisNotificationList = majlisNotificationList;
    }

    public MajlisMdCode getStatus() {
        return status;
    }

    public void setStatus(MajlisMdCode status) {
        this.status = status;
    }

    @XmlTransient
    public List<MajlisGroup> getMajlisGroupList() {
        return majlisGroupList;
    }

    public void setMajlisGroupList(List<MajlisGroup> majlisGroupList) {
        this.majlisGroupList = majlisGroupList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (userId != null ? userId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof MajlisCmsUsers)) {
            return false;
        }
        MajlisCmsUsers other = (MajlisCmsUsers) object;
        if ((this.userId == null && other.userId != null) || (this.userId != null && !this.userId.equals(other.userId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.dmssw.orm.models.MajlisCmsUsers[ userId=" + userId + " ]";
    }
    
}
