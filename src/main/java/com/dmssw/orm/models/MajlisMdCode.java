/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dmssw.orm.models;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Shanka
 */
@Entity
@Table(name = "majlis_md_code")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "MajlisMdCode.findAll", query = "SELECT m FROM MajlisMdCode m"),
    @NamedQuery(name = "MajlisMdCode.findByCodeId", query = "SELECT m FROM MajlisMdCode m WHERE m.codeId = :codeId"),
    @NamedQuery(name = "MajlisMdCode.findByCodeType", query = "SELECT m FROM MajlisMdCode m WHERE m.codeType = :codeType"),
    @NamedQuery(name = "MajlisMdCode.findByCodeSubType", query = "SELECT m FROM MajlisMdCode m WHERE m.codeSubType = :codeSubType"),
    @NamedQuery(name = "MajlisMdCode.findByCodeLocale", query = "SELECT m FROM MajlisMdCode m WHERE m.codeLocale = :codeLocale"),
    @NamedQuery(name = "MajlisMdCode.findByCodeSeq", query = "SELECT m FROM MajlisMdCode m WHERE m.codeSeq = :codeSeq"),
    @NamedQuery(name = "MajlisMdCode.findByCodeMessage", query = "SELECT m FROM MajlisMdCode m WHERE m.codeMessage = :codeMessage")})
public class MajlisMdCode implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "CODE_ID")
    private Integer codeId;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "CODE_TYPE")
    private String codeType;
    @Size(max = 2000)
    @Column(name = "CODE_SUB_TYPE")
    private String codeSubType;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 20)
    @Column(name = "CODE_LOCALE")
    private String codeLocale;
    @Basic(optional = false)
    @NotNull
    @Column(name = "CODE_SEQ")
    private int codeSeq;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 2000)
    @Column(name = "CODE_MESSAGE")
    private String codeMessage;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "eventStatus")
    private List<MajlisGroupEvent> majlisGroupEventList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "notificationStatus")
    private List<MajlisNotification> majlisNotificationList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "categoryStatus")
    private List<MajlisCategory> majlisCategoryList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "status")
    private List<MajlisCmsUsers> majlisCmsUsersList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "templateStatus")
    private List<MajlisBirthdayTemplate> majlisBirthdayTemplateList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "groupPublicLevel")
    private List<MajlisGroup> majlisGroupList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "groupStatus")
    private List<MajlisGroup> majlisGroupList1;

    public MajlisMdCode() {
    }

    public MajlisMdCode(Integer codeId) {
        this.codeId = codeId;
    }

    public MajlisMdCode(Integer codeId, String codeType, String codeLocale, int codeSeq, String codeMessage) {
        this.codeId = codeId;
        this.codeType = codeType;
        this.codeLocale = codeLocale;
        this.codeSeq = codeSeq;
        this.codeMessage = codeMessage;
    }

    public Integer getCodeId() {
        return codeId;
    }

    public void setCodeId(Integer codeId) {
        this.codeId = codeId;
    }

    public String getCodeType() {
        return codeType;
    }

    public void setCodeType(String codeType) {
        this.codeType = codeType;
    }

    public String getCodeSubType() {
        return codeSubType;
    }

    public void setCodeSubType(String codeSubType) {
        this.codeSubType = codeSubType;
    }

    public String getCodeLocale() {
        return codeLocale;
    }

    public void setCodeLocale(String codeLocale) {
        this.codeLocale = codeLocale;
    }

    public int getCodeSeq() {
        return codeSeq;
    }

    public void setCodeSeq(int codeSeq) {
        this.codeSeq = codeSeq;
    }

    public String getCodeMessage() {
        return codeMessage;
    }

    public void setCodeMessage(String codeMessage) {
        this.codeMessage = codeMessage;
    }

    @XmlTransient
    public List<MajlisGroupEvent> getMajlisGroupEventList() {
        return majlisGroupEventList;
    }

    public void setMajlisGroupEventList(List<MajlisGroupEvent> majlisGroupEventList) {
        this.majlisGroupEventList = majlisGroupEventList;
    }

    @XmlTransient
    public List<MajlisNotification> getMajlisNotificationList() {
        return majlisNotificationList;
    }

    public void setMajlisNotificationList(List<MajlisNotification> majlisNotificationList) {
        this.majlisNotificationList = majlisNotificationList;
    }

    @XmlTransient
    public List<MajlisCategory> getMajlisCategoryList() {
        return majlisCategoryList;
    }

    public void setMajlisCategoryList(List<MajlisCategory> majlisCategoryList) {
        this.majlisCategoryList = majlisCategoryList;
    }

    @XmlTransient
    public List<MajlisCmsUsers> getMajlisCmsUsersList() {
        return majlisCmsUsersList;
    }

    public void setMajlisCmsUsersList(List<MajlisCmsUsers> majlisCmsUsersList) {
        this.majlisCmsUsersList = majlisCmsUsersList;
    }

    @XmlTransient
    public List<MajlisBirthdayTemplate> getMajlisBirthdayTemplateList() {
        return majlisBirthdayTemplateList;
    }

    public void setMajlisBirthdayTemplateList(List<MajlisBirthdayTemplate> majlisBirthdayTemplateList) {
        this.majlisBirthdayTemplateList = majlisBirthdayTemplateList;
    }

    @XmlTransient
    public List<MajlisGroup> getMajlisGroupList() {
        return majlisGroupList;
    }

    public void setMajlisGroupList(List<MajlisGroup> majlisGroupList) {
        this.majlisGroupList = majlisGroupList;
    }

    @XmlTransient
    public List<MajlisGroup> getMajlisGroupList1() {
        return majlisGroupList1;
    }

    public void setMajlisGroupList1(List<MajlisGroup> majlisGroupList1) {
        this.majlisGroupList1 = majlisGroupList1;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (codeId != null ? codeId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof MajlisMdCode)) {
            return false;
        }
        MajlisMdCode other = (MajlisMdCode) object;
        if ((this.codeId == null && other.codeId != null) || (this.codeId != null && !this.codeId.equals(other.codeId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.dmssw.orm.models.MajlisMdCode[ codeId=" + codeId + " ]";
    }
    
}
