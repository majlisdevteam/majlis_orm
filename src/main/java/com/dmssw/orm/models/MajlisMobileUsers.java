/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dmssw.orm.models;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Shanka
 */
@Entity
@Table(name = "majlis_mobile_users")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "MajlisMobileUsers.findAll", query = "SELECT m FROM MajlisMobileUsers m"),
    @NamedQuery(name = "MajlisMobileUsers.findByMobileUserId", query = "SELECT m FROM MajlisMobileUsers m WHERE m.mobileUserId = :mobileUserId"),
    @NamedQuery(name = "MajlisMobileUsers.findByMobileUserMobileNo", query = "SELECT m FROM MajlisMobileUsers m WHERE m.mobileUserMobileNo = :mobileUserMobileNo"),
    @NamedQuery(name = "MajlisMobileUsers.findByMobileUserName", query = "SELECT m FROM MajlisMobileUsers m WHERE m.mobileUserName = :mobileUserName"),
    @NamedQuery(name = "MajlisMobileUsers.findByMobileUserEmail", query = "SELECT m FROM MajlisMobileUsers m WHERE m.mobileUserEmail = :mobileUserEmail"),
    @NamedQuery(name = "MajlisMobileUsers.findByMobileUserDob", query = "SELECT m FROM MajlisMobileUsers m WHERE m.mobileUserDob = :mobileUserDob"),
    @NamedQuery(name = "MajlisMobileUsers.findByMobileUserCountry", query = "SELECT m FROM MajlisMobileUsers m WHERE m.mobileUserCountry = :mobileUserCountry"),
    @NamedQuery(name = "MajlisMobileUsers.findByMobileUserProvince", query = "SELECT m FROM MajlisMobileUsers m WHERE m.mobileUserProvince = :mobileUserProvince"),
    @NamedQuery(name = "MajlisMobileUsers.findByMobileUserCity", query = "SELECT m FROM MajlisMobileUsers m WHERE m.mobileUserCity = :mobileUserCity"),
    @NamedQuery(name = "MajlisMobileUsers.findByMobileUserDateRegistration", query = "SELECT m FROM MajlisMobileUsers m WHERE m.mobileUserDateRegistration = :mobileUserDateRegistration"),
    @NamedQuery(name = "MajlisMobileUsers.findByMobileUserDateModified", query = "SELECT m FROM MajlisMobileUsers m WHERE m.mobileUserDateModified = :mobileUserDateModified"),
    @NamedQuery(name = "MajlisMobileUsers.findByMobileUserPassCode", query = "SELECT m FROM MajlisMobileUsers m WHERE m.mobileUserPassCode = :mobileUserPassCode")})
public class MajlisMobileUsers implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "MOBILE_USER_ID")
    private Integer mobileUserId;
    @Size(max = 15)
    @Column(name = "MOBILE_USER_MOBILE_NO")
    private String mobileUserMobileNo;
    @Size(max = 2000)
    @Column(name = "MOBILE_USER_NAME")
    private String mobileUserName;
    @Size(max = 20)
    @Column(name = "MOBILE_USER_EMAIL")
    private String mobileUserEmail;
    @Column(name = "MOBILE_USER_DOB")
    @Temporal(TemporalType.DATE)
    private Date mobileUserDob;
    @Size(max = 20)
    @Column(name = "MOBILE_USER_COUNTRY")
    private String mobileUserCountry;
    @Size(max = 100)
    @Column(name = "MOBILE_USER_PROVINCE")
    private String mobileUserProvince;
    @Size(max = 100)
    @Column(name = "MOBILE_USER_CITY")
    private String mobileUserCity;
    @Column(name = "MOBILE_USER_DATE_REGISTRATION")
    @Temporal(TemporalType.TIMESTAMP)
    private Date mobileUserDateRegistration;
    @Column(name = "MOBILE_USER_DATE_MODIFIED")
    @Temporal(TemporalType.TIMESTAMP)
    private Date mobileUserDateModified;
    @Size(max = 2000)
    @Column(name = "MOBILE_USER_PASS_CODE")
    private String mobileUserPassCode;
    @ManyToMany(mappedBy = "majlisMobileUsersList")
    private List<MajlisCategory> majlisCategoryList;
    @ManyToMany(mappedBy = "majlisMobileUsersList")
    private List<MajlisGroup> majlisGroupList;

    public MajlisMobileUsers() {
    }

    public MajlisMobileUsers(Integer mobileUserId) {
        this.mobileUserId = mobileUserId;
    }

    public Integer getMobileUserId() {
        return mobileUserId;
    }

    public void setMobileUserId(Integer mobileUserId) {
        this.mobileUserId = mobileUserId;
    }

    public String getMobileUserMobileNo() {
        return mobileUserMobileNo;
    }

    public void setMobileUserMobileNo(String mobileUserMobileNo) {
        this.mobileUserMobileNo = mobileUserMobileNo;
    }

    public String getMobileUserName() {
        return mobileUserName;
    }

    public void setMobileUserName(String mobileUserName) {
        this.mobileUserName = mobileUserName;
    }

    public String getMobileUserEmail() {
        return mobileUserEmail;
    }

    public void setMobileUserEmail(String mobileUserEmail) {
        this.mobileUserEmail = mobileUserEmail;
    }

    public Date getMobileUserDob() {
        return mobileUserDob;
    }

    public void setMobileUserDob(Date mobileUserDob) {
        this.mobileUserDob = mobileUserDob;
    }

    public String getMobileUserCountry() {
        return mobileUserCountry;
    }

    public void setMobileUserCountry(String mobileUserCountry) {
        this.mobileUserCountry = mobileUserCountry;
    }

    public String getMobileUserProvince() {
        return mobileUserProvince;
    }

    public void setMobileUserProvince(String mobileUserProvince) {
        this.mobileUserProvince = mobileUserProvince;
    }

    public String getMobileUserCity() {
        return mobileUserCity;
    }

    public void setMobileUserCity(String mobileUserCity) {
        this.mobileUserCity = mobileUserCity;
    }

    public Date getMobileUserDateRegistration() {
        return mobileUserDateRegistration;
    }

    public void setMobileUserDateRegistration(Date mobileUserDateRegistration) {
        this.mobileUserDateRegistration = mobileUserDateRegistration;
    }

    public Date getMobileUserDateModified() {
        return mobileUserDateModified;
    }

    public void setMobileUserDateModified(Date mobileUserDateModified) {
        this.mobileUserDateModified = mobileUserDateModified;
    }

    public String getMobileUserPassCode() {
        return mobileUserPassCode;
    }

    public void setMobileUserPassCode(String mobileUserPassCode) {
        this.mobileUserPassCode = mobileUserPassCode;
    }

    @XmlTransient
    public List<MajlisCategory> getMajlisCategoryList() {
        return majlisCategoryList;
    }

    public void setMajlisCategoryList(List<MajlisCategory> majlisCategoryList) {
        this.majlisCategoryList = majlisCategoryList;
    }

    @XmlTransient
    public List<MajlisGroup> getMajlisGroupList() {
        return majlisGroupList;
    }

    public void setMajlisGroupList(List<MajlisGroup> majlisGroupList) {
        this.majlisGroupList = majlisGroupList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (mobileUserId != null ? mobileUserId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof MajlisMobileUsers)) {
            return false;
        }
        MajlisMobileUsers other = (MajlisMobileUsers) object;
        if ((this.mobileUserId == null && other.mobileUserId != null) || (this.mobileUserId != null && !this.mobileUserId.equals(other.mobileUserId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.dmssw.orm.models.MajlisMobileUsers[ mobileUserId=" + mobileUserId + " ]";
    }
    
}
