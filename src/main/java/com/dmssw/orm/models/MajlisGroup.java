/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dmssw.orm.models;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Shanka
 */
@Entity
@Table(name = "majlis_group")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "MajlisGroup.findAll", query = "SELECT m FROM MajlisGroup m"),
    @NamedQuery(name = "MajlisGroup.findByGroupId", query = "SELECT m FROM MajlisGroup m WHERE m.groupId = :groupId"),
    @NamedQuery(name = "MajlisGroup.findByGroupSystemId", query = "SELECT m FROM MajlisGroup m WHERE m.groupSystemId = :groupSystemId"),
    @NamedQuery(name = "MajlisGroup.findByGroupTitle", query = "SELECT m FROM MajlisGroup m WHERE m.groupTitle = :groupTitle"),
    @NamedQuery(name = "MajlisGroup.findByGroupIconPath", query = "SELECT m FROM MajlisGroup m WHERE m.groupIconPath = :groupIconPath"),
    @NamedQuery(name = "MajlisGroup.findByGroupDescription", query = "SELECT m FROM MajlisGroup m WHERE m.groupDescription = :groupDescription"),
    @NamedQuery(name = "MajlisGroup.findByGroupCountry", query = "SELECT m FROM MajlisGroup m WHERE m.groupCountry = :groupCountry"),
    @NamedQuery(name = "MajlisGroup.findByGroupDistrict", query = "SELECT m FROM MajlisGroup m WHERE m.groupDistrict = :groupDistrict"),
    @NamedQuery(name = "MajlisGroup.findByGroupCity", query = "SELECT m FROM MajlisGroup m WHERE m.groupCity = :groupCity"),
    @NamedQuery(name = "MajlisGroup.findByUserInserted", query = "SELECT m FROM MajlisGroup m WHERE m.userInserted = :userInserted"),
    @NamedQuery(name = "MajlisGroup.findByDateInserted", query = "SELECT m FROM MajlisGroup m WHERE m.dateInserted = :dateInserted"),
    @NamedQuery(name = "MajlisGroup.findByUserModified", query = "SELECT m FROM MajlisGroup m WHERE m.userModified = :userModified"),
    @NamedQuery(name = "MajlisGroup.findByDateModified", query = "SELECT m FROM MajlisGroup m WHERE m.dateModified = :dateModified")})
public class MajlisGroup implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "GROUP_ID")
    private Integer groupId;
    @Basic(optional = false)
    @NotNull
    @Column(name = "GROUP_SYSTEM_ID")
    private int groupSystemId;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 5500)
    @Column(name = "GROUP_TITLE")
    private String groupTitle;
    @Size(max = 2000)
    @Column(name = "GROUP_ICON_PATH")
    private String groupIconPath;
    @Size(max = 2000)
    @Column(name = "GROUP_DESCRIPTION")
    private String groupDescription;
    @Size(max = 100)
    @Column(name = "GROUP_COUNTRY")
    private String groupCountry;
    @Size(max = 10)
    @Column(name = "GROUP_DISTRICT")
    private String groupDistrict;
    @Size(max = 100)
    @Column(name = "GROUP_CITY")
    private String groupCity;
    @Size(max = 50)
    @Column(name = "USER_INSERTED")
    private String userInserted;
    @Column(name = "DATE_INSERTED")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateInserted;
    @Size(max = 50)
    @Column(name = "USER_MODIFIED")
    private String userModified;
    @Column(name = "DATE_MODIFIED")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateModified;
    @JoinTable(name = "majlis_mobile_user_group", joinColumns = {
        @JoinColumn(name = "GROUP_ID", referencedColumnName = "GROUP_ID")}, inverseJoinColumns = {
        @JoinColumn(name = "USER_ID", referencedColumnName = "MOBILE_USER_ID")})
    @ManyToMany
    private List<MajlisMobileUsers> majlisMobileUsersList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "groupId")
    private List<MajlisGroupEvent> majlisGroupEventList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "notificationGroupId")
    private List<MajlisNotification> majlisNotificationList;
    @JoinColumn(name = "GROUP_ADMIN", referencedColumnName = "USER_ID")
    @ManyToOne
    private MajlisCmsUsers groupAdmin;
    @JoinColumn(name = "GROUP_PUBLIC_LEVEL", referencedColumnName = "CODE_ID")
    @ManyToOne(optional = false)
    private MajlisMdCode groupPublicLevel;
    @JoinColumn(name = "GROUP_STATUS", referencedColumnName = "CODE_ID")
    @ManyToOne(optional = false)
    private MajlisMdCode groupStatus;

    public MajlisGroup() {
    }

    public MajlisGroup(Integer groupId) {
        this.groupId = groupId;
    }

    public MajlisGroup(Integer groupId, int groupSystemId, String groupTitle) {
        this.groupId = groupId;
        this.groupSystemId = groupSystemId;
        this.groupTitle = groupTitle;
    }

    public Integer getGroupId() {
        return groupId;
    }

    public void setGroupId(Integer groupId) {
        this.groupId = groupId;
    }

    public int getGroupSystemId() {
        return groupSystemId;
    }

    public void setGroupSystemId(int groupSystemId) {
        this.groupSystemId = groupSystemId;
    }

    public String getGroupTitle() {
        return groupTitle;
    }

    public void setGroupTitle(String groupTitle) {
        this.groupTitle = groupTitle;
    }

    public String getGroupIconPath() {
        return groupIconPath;
    }

    public void setGroupIconPath(String groupIconPath) {
        this.groupIconPath = groupIconPath;
    }

    public String getGroupDescription() {
        return groupDescription;
    }

    public void setGroupDescription(String groupDescription) {
        this.groupDescription = groupDescription;
    }

    public String getGroupCountry() {
        return groupCountry;
    }

    public void setGroupCountry(String groupCountry) {
        this.groupCountry = groupCountry;
    }

    public String getGroupDistrict() {
        return groupDistrict;
    }

    public void setGroupDistrict(String groupDistrict) {
        this.groupDistrict = groupDistrict;
    }

    public String getGroupCity() {
        return groupCity;
    }

    public void setGroupCity(String groupCity) {
        this.groupCity = groupCity;
    }

    public String getUserInserted() {
        return userInserted;
    }

    public void setUserInserted(String userInserted) {
        this.userInserted = userInserted;
    }

    public Date getDateInserted() {
        return dateInserted;
    }

    public void setDateInserted(Date dateInserted) {
        this.dateInserted = dateInserted;
    }

    public String getUserModified() {
        return userModified;
    }

    public void setUserModified(String userModified) {
        this.userModified = userModified;
    }

    public Date getDateModified() {
        return dateModified;
    }

    public void setDateModified(Date dateModified) {
        this.dateModified = dateModified;
    }

    @XmlTransient
    public List<MajlisMobileUsers> getMajlisMobileUsersList() {
        return majlisMobileUsersList;
    }

    public void setMajlisMobileUsersList(List<MajlisMobileUsers> majlisMobileUsersList) {
        this.majlisMobileUsersList = majlisMobileUsersList;
    }

    @XmlTransient
    public List<MajlisGroupEvent> getMajlisGroupEventList() {
        return majlisGroupEventList;
    }

    public void setMajlisGroupEventList(List<MajlisGroupEvent> majlisGroupEventList) {
        this.majlisGroupEventList = majlisGroupEventList;
    }

    @XmlTransient
    public List<MajlisNotification> getMajlisNotificationList() {
        return majlisNotificationList;
    }

    public void setMajlisNotificationList(List<MajlisNotification> majlisNotificationList) {
        this.majlisNotificationList = majlisNotificationList;
    }

    public MajlisCmsUsers getGroupAdmin() {
        return groupAdmin;
    }

    public void setGroupAdmin(MajlisCmsUsers groupAdmin) {
        this.groupAdmin = groupAdmin;
    }

    public MajlisMdCode getGroupPublicLevel() {
        return groupPublicLevel;
    }

    public void setGroupPublicLevel(MajlisMdCode groupPublicLevel) {
        this.groupPublicLevel = groupPublicLevel;
    }

    public MajlisMdCode getGroupStatus() {
        return groupStatus;
    }

    public void setGroupStatus(MajlisMdCode groupStatus) {
        this.groupStatus = groupStatus;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (groupId != null ? groupId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof MajlisGroup)) {
            return false;
        }
        MajlisGroup other = (MajlisGroup) object;
        if ((this.groupId == null && other.groupId != null) || (this.groupId != null && !this.groupId.equals(other.groupId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.dmssw.orm.models.MajlisGroup[ groupId=" + groupId + " ]";
    }
    
}
