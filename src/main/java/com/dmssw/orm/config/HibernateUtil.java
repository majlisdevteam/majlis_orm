/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dmssw.orm.config;

import org.hibernate.HibernateException;
import org.hibernate.SessionFactory;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;

/**
 * Hibernate Utility class with a convenient method to get Session Factory
 * object.
 *
 * @author Shanka
 */
public class HibernateUtil {

  
    private static SessionFactory sessionFactory = buildSessionFactory();

    private static SessionFactory buildSessionFactory() {
        try {

            if (sessionFactory == null) {
                Configuration configuration = new Configuration().configure();
//                configuration.setProperty("hibernate.connection.url", AppParams.CONNECTION_MYSQL);
//                configuration.setProperty("hibernate.connection.username", AppParams.DB_USER_NAME);
//                configuration.setProperty("hibernate.connection.password", AppParams.DB_PASSWORD);
                configuration.setProperty("hibernate.connection.datasource", AppParams.JNDI_DB);
                StandardServiceRegistryBuilder serviceRegistryBuilder = new StandardServiceRegistryBuilder();

                serviceRegistryBuilder.applySettings(configuration.getProperties());

                sessionFactory = configuration.buildSessionFactory();

            }
            return sessionFactory;

        } catch (HibernateException e) {
            System.out.println("Session factory creation was failed. Error : " + e.getMessage());
            e.printStackTrace();
            throw new ExceptionInInitializerError(e);
        }
    }

    public static SessionFactory getSessionFactory() {
        return sessionFactory;
    }

    public static void shutdown() {
        getSessionFactory().close();
    }
}
