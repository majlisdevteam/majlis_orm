/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dmssw.orm.config;

import java.io.FileInputStream;
import java.util.Properties;

/**
 *
 * @author Shanka
 */
public class AppParams {

    static Properties props = new Properties();
    static String wspath = System.getenv("WS_HOME");

    static String pathset = wspath.replace("\\", "/");
    static String ippath = pathset + "/Majlis_DB_Conn.properties";

    public static final String CONNECTION_MYSQL;
    public static final String DRIVER;
    public static final String DB_CONNECTION_TYPE;

    public static final String JNDI_DB;

    static {

        System.out.println("Hibernate module Majlis property path " + ippath);

        try {

            props.load(new FileInputStream(ippath));

        } catch (Exception e) {
            System.out.println("Hibernate module Majlis cannot read properties.....");
            e.printStackTrace();
        }

        DRIVER = props.getProperty("DRIVER");
        DB_CONNECTION_TYPE = props.getProperty("DB_CONNECTION_TYPE");
        CONNECTION_MYSQL = props.getProperty("CONNECTION_MYSQL");
        JNDI_DB = props.getProperty("JNDI_DB_CONN");

    }
}
